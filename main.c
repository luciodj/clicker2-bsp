/**
  File Name:
    main.c

  Summary:
 Board Support Package for Clicker 2 w.PIC18F87J50

  Description:
    Device            :  PIC18F87J50
    Compiler          :  XC8 1.35
    MPLAB             :  MPLAB X 3.20
*/

/*
    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

#include "mcc_generated_files/mcc.h"
#include "oled.h"

// IR Thermo Click(tm) constants
#define IR_THERMO_ADDRESS   0x5A
#define AMB_TEMP            0x06
#define OBJ_TEMP            0x07

bool IR_SensorRead(uint8_t reg, float * pTemp)
{
    int16_t data;
    I2C_MESSAGE_STATUS status = I2C_MESSAGE_PENDING;
    static I2C_TRANSACTION_REQUEST_BLOCK trb[2];
    
    I2C_MasterWriteTRBBuild(&trb[0], &reg, 1, IR_THERMO_ADDRESS);
    I2C_MasterReadTRBBuild(&trb[1], (uint8_t*)&data, 2, IR_THERMO_ADDRESS);                
    I2C_MasterTRBInsert(2, &trb[0], &status);
    
    while(status == I2C_MESSAGE_PENDING);          // blocking
    *pTemp = ((float)(data) *  0.02) - 273.15;     // convert to deg C

    return (status == I2C_MESSAGE_COMPLETE); 
} 

void delay_ms( uint16_t time)
{
    while(time--){
        __delay_ms(1);
    }
}

void main(void)
{
    // Initialize the device
    SYSTEM_Initialize();
    INTERRUPT_GlobalInterruptEnable();
    INTERRUPT_PeripheralInterruptEnable();
    OLED_Initialize();
    
    OLED_Clear();
    OLED_SetScale(4,4);
    while (1)
    {
        char s[6];
        float temp1, temp2;
        LED1_Toggle();
        IR_SensorRead(OBJ_TEMP, &temp1);
//        IR_SensorRead(AMB_TEMP, &temp2);
        sprintf(s, "%2.1f", temp1);
        OLED_Puts(0,0, s);
        delay_ms(1000);
    }
}
